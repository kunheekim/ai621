%% Initials (5 points)

% Load the image.
im = imread("data/banana_slug.tiff");

% Check the class and size of the image.
cls = class(im);
[h, w] = size(im);
fprintf("Class of an image: %s\n", cls);
fprintf("bits/integer: %s\n", replace(cls, "uint", ""));
fprintf("Height: %i\n", h);
fprintf("Width: %i\n", w);

% Convert the image into a double-precision array.
im = double(im);
fprintf("Class of an image: %s\n", class(im));


%% Linearization (5 points)

black_level = 2047;
saturation = 15000;

% Apply linear transformation.
im = (im - black_level) / (saturation - black_level);

% Clip values outside range.
im = max(im, 0);
im = min(im, 1);
imshow(min(1, im * 5));


%% Identifying the correct Bayer pattern (20 points)

% create four sub-images
im1 = im(1:2:end, 1:2:end);
im2 = im(1:2:end, 2:2:end);
im3 = im(2:2:end, 1:2:end);
im4 = im(2:2:end, 2:2:end);


% im = cat(3, red, green, blue)
im_grbg = cat(3, im2, im1, im3); % grbg
im_rggb = cat(3, im1, im2, im4); % rggb
im_bggr = cat(3, im4, im2, im1); % bggr
im_gbrg = cat(3, im3, im1, im2); % gbrg

% Display all images.
x1 = 1200; x2 = 1700;
y1 = 800; y2 = 1150;
figure;
subplot(2, 2, 1), imshow(min(1, im_grbg(y1:y2, x1:x2, :) * 6)); title("GRBG");
subplot(2, 2, 2), imshow(min(1, im_rggb(y1:y2, x1:x2, :) * 6)); title("RGGB"); % banana is yellow!
subplot(2, 2, 3), imshow(min(1, im_bggr(y1:y2, x1:x2, :) * 6)); title("BGGR");
subplot(2, 2, 4), imshow(min(1, im_gbrg(y1:y2, x1:x2, :) * 6)); title("GBRG");


%% White balancing (20 points)

im_rgb = cat(3, im1, 0.5*(im2+im3), im4); % Here, we also consider second green sub-image.
% See the end of this script for the implementation of this function.
im_grey = automatic_white_banalcing(im_rgb, "grey");
im_white = automatic_white_banalcing(im_rgb, "white");

figure;
subplot(1, 2, 1), imshow(min(1, im_grey * 5)); title("Gray world assumption");
subplot(1, 2, 2), imshow(min(1, im_white * 5)); title("White world assumption");

im_wb = im_grey;


%% Demosaicing (20 points)

im_red = im_wb(:, :, 1);
im_green = im_wb(:, :, 2);
im_blue = im_wb(:, :, 3);

interp_red = interp2(im_red);
interp_green = interp2(im_green);
interp_blue = interp2(im_blue);

im_rgb = cat(3, interp_red, interp_green, interp_blue);

figure; imshow(min(1, im_rgb * 5)); title("Demosiced image");


%% Brightness adjustment and gamma correction (20 points)

im_gray = rgb2gray(im_rgb);

figure;
for i = 1:16
    scale = i * 0.5 + 0.5;
    im_scaled = im_gray * scale;
    subplot(4, 4, i); imshow(min(1, im_scaled));
    title("Scale = " + scale);
end

im_scaled = im_rgb * 5.5;
im_final = min(1, gamma_correction(im_scaled));

figure;
subplot(1, 2, 1); imshow(im_scaled); title("Before gamma correction");
subplot(1, 2, 2); imshow(im_final); title("After gamma correction");


%% Compression (5 points)
imwrite(im_final, "im_lossless.png");
imwrite(im_final, "im_quality_95.jpeg", "Quality", 95);

imwrite(im_final, "im_quality_50.jpeg", "Quality", 50);
imwrite(im_final, "im_quality_20.jpeg", "Quality", 20);
imwrite(im_final, "im_quality_15.jpeg", "Quality", 15);
imwrite(im_final, "im_quality_10.jpeg", "Quality", 10);
imwrite(im_final, "im_quality_05.jpeg", "Quality", 5);


%% Automatic white balancing function.
function balanced_rgb = automatic_white_banalcing(sensor_rgb, world)
    % Create the normalization matrix.
    if world == "grey" || world == "gray"
        R_avg = mean(sensor_rgb(:,:,1), "all");
        G_avg = mean(sensor_rgb(:,:,2), "all");
        B_avg = mean(sensor_rgb(:,:,3), "all");
        norm_matrix = [G_avg/R_avg 0 0; 0 1 0; 0 0 G_avg/B_avg];
    elseif world == "white"
        R_max = max(sensor_rgb(:,:,1), [], "all");
        G_max = max(sensor_rgb(:,:,2), [], "all");
        B_max = max(sensor_rgb(:,:,3), [], "all");
        norm_matrix = [G_max/R_max 0 0; 0 1 0; 0 0 G_max/B_max];
    else
        error("Unsupported world assumption! Choose from 'gray', 'grey' or 'white'.")
    end

    % Reshape an image for matrix multiplication.
    [h, w, ~] = size(sensor_rgb);
    flat_rgb = reshape(sensor_rgb, h*w, 3);
    % Apply normalization.
    balanced_rgb = flat_rgb * norm_matrix;
    % Reshape back to the original shape.
    balanced_rgb = reshape(balanced_rgb, [h, w, 3]);
end


function non_linear = gamma_correction(linear)
    non_linear = linear;
    non_linear(linear <= 0.0031308) = 12.92 * linear(linear <= 0.0031308);
    non_linear(linear >= 0.0031308) = (1+0.055) * linear(linear >= 0.0031308) .^ (1/2.4) - 0.055;
end
